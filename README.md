
## Description

This project includes the code for apis to create, get, search for a student in a linked  mysql database. Below is the Postman collection which implements the above apis.

## Postman Collection

[![Run in Postman](https://run.pstmn.io/button.svg)](https://www.getpostman.com/collections/8bcafeba169d6c312542)