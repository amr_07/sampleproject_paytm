package com.amr;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
	
	// custom query to search Student list by name 
    //List<Student> findBySubStringLike(String text);
    //Student findOne(int id);
    //Student delete(int id);
	
	@Query("SELECT s FROM Student s WHERE LOWER (s.name) LIKE LOWER(CONCAT('%',:searchTerm,'%'))")
	List<Student> findBySearchTerm(@Param("searchTerm") String searchTerm);

}
