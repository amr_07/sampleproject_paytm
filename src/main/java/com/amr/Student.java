package com.amr;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Student {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)	
	private int id;
	
	private String name;
    private int cls;
    
    
    public Student() {  }
    
    public Student(String name, int cls) {
    	this.setName(name);
    	this.setCls(cls);
    }

    public Student(int id,String name,int cls) {
    	this.setId(id);
    	this.setName(name);
        this.setCls(cls);
    }
    
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCls() {
		return cls;
	}
	public void setCls(int cls) {
		this.cls = cls;
	}
	
    
	@Override
    public String toString() {
        return "Student{" +
                "Rollno=" + id +
                ", name='" + name + '\'' +
                ", cls='" + cls + '\'' +
                '}';
    }    
    

}
