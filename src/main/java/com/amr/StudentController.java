package com.amr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class StudentController {
	//StudentMockData studentmockdata = StudentMockData.getInstance();
	
	@Autowired
	StudentRepository studentrepository;

    @GetMapping("/student")
    public List<Student> index(){
        return studentrepository.findAll();
    }

    @GetMapping("/student/{id}")
    public Student show(@PathVariable String id){
        int stId = Integer.parseInt(id);
        return studentrepository.findById(stId).get();
    }
    
  @GetMapping("/student/substringsearch/{text}")
  public List<Student> substringsearch(@PathVariable String text){
      //String searchstr = text;
      return studentrepository.findBySearchTerm(text);
  }
    
    
    
//    @GetMapping("/student/substringsearch/{text}")
//    public List<Student> substringsearch(@PathVariable String text){
//        //String searchstr = text;
//        return studentrepository.findBySubStringLike("%text%");
//    }


    @PostMapping("/student")
    public Student create(@RequestBody Map<String, String> body){
        //int stId = Integer.parseInt(body.get("rollno"));
        String sname = body.get("name");
        int scls = Integer.parseInt(body.get("cls"));
        return studentrepository.save(new Student(sname,scls));
    }

    @PutMapping("/student/{id}")
    public Student update(@PathVariable String id, @RequestBody Map<String, String> body){
        int stId = Integer.parseInt(id);
        
        Student p = studentrepository.findById(stId).get();
        p.setName(body.get("name"));
        //String sname = body.get("name");
        int scls = Integer.parseInt(body.get("cls"));
        p.setCls(scls);
        return studentrepository.save(p);
    }

    @DeleteMapping("student/{id}")
    public boolean delete(@PathVariable String rollno){
        int stId = Integer.parseInt(rollno);
        studentrepository.deleteById(stId);
        return true;
    }

}
