package com.amr;

import java.util.ArrayList;
import java.util.List;

public class StudentMockData {
	
	private List<Student> st;

    private static StudentMockData instance = null;
    public static StudentMockData getInstance(){
         if(instance == null){
             instance = new StudentMockData();
         }
         return instance;
    }


    public StudentMockData(){
        st = new ArrayList<Student>();
        st.add(new Student(1,"Amitabh Bachchan",12 ));
        st.add(new Student(2,"Hrithik Roshan",11 ));
        st.add(new Student(3,"Shahrukh Khan",10 ));
        st.add(new Student(4,"Farhan Akhtar",9 ));
        st.add(new Student(5,"Ranbir Kapoor",9 ));
        
    }

    // return all blogs
    public List<Student> fetchStudents() {
        return st;
    }

    // return blog by id
    public Student getStudentById(int id) {
        for(Student s: st) {
            if(s.getId() == id) {
                return s;
            }
        }
        return null;
    }

    // search blog by text
    public List<Student> searchStudents(String searchstr) {
        List<Student> searchedStudents = new ArrayList<Student>();
        for(Student s: st) {
            if(s.getName().toLowerCase().contains(searchstr.toLowerCase())) {
                searchedStudents.add(s);
            }
        }

        return searchedStudents;
    }

    // Add to Student list
    public Student createStudent(int rollno, String name, int cls) {
        Student newSt = new Student(rollno,name,cls);
        st.add(newSt);
        return newSt;
    }

    // update Student list
    public Student updateStudent(int rollno, String name, int cls) {
        for(Student s: st) {
            if(s.getId() == rollno) {
                int stIndex = st.indexOf(s);
                s.setName(name);
                s.setCls(cls);
                st.set(stIndex, s);
                return s;
            }

        }

        return null;
    }

    // delete Student by Rollno
    public boolean delete(int rollno){
        int stIndex = -1;
        for(Student b: st) {
            if(b.getId() == rollno) {
                stIndex = st.indexOf(b);
                continue;
            }
        }
        if(stIndex > -1){
            st.remove(stIndex);
        }
        return true;
    }

}
